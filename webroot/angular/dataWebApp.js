// Generated by CoffeeScript 1.8.0
(function() {
  var dataWebApp;

  dataWebApp = angular.module('dataWebApp', ['ngRoute', 'ngResource', 'IndexCtrl', 'IndexService']);

  dataWebApp.config([
    '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
      return $routeProvider.when('/', {
        templateUrl: 'webroot/view/homePage.html'
      });
    }
  ]);

}).call(this);
