dataWebApp = angular.module('dataWebApp', ['ngRoute', 'ngResource', 'IndexCtrl', 'IndexService'])

dataWebApp.config ['$routeProvider', '$locationProvider', ($routeProvider, $locationProvider) ->

  $routeProvider.when '/', {templateUrl: 'webroot/view/homePage.html'}
]