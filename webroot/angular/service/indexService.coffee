indexService = angular.module 'IndexService', ['ngResource']

indexService.factory 'familiesSrv',['$resource', ($resource) ->
  $resource 'api/family/:depth', {depth: '@depth'}, {query: {method: 'GET', isArray:true}}
]

indexService.factory 'articlesSrv', ['$resource', ($resource) ->
  $resource 'api/articles/:id_family', {id_family: '@id_family'}, {query: {method:'GET', isArray:true}}
]