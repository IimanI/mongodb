<?php
require_once 'vendor/autoload.php';

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

//routing section

$app = new Silex\Application();

//active debug
$app['debug'] = true;

// route homepage

$app->get('/', 'mongodb\controller\\AppController::viewHome');

// API
// récup une famille en fonction de son niveau
$app->get('/api/family/{depth}', function($depth) use ($app) {
    $ctrl = new \mongodb\controller\AppController();
    $families = $ctrl->FamilyByDepth($depth, $app);
    return $families;
});

//récup tous les articles d'une famille
$app->get('/api/articles/{id_family}', function($id_family) use ($app) {
    $ctrl = new \mongodb\controller\AppController();
    $articles = $ctrl->getArticlesByFamily($id_family, $app);
    return $articles;
});



$app->run();