<?php

namespace mongodb\controller;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

class AppController {

    public function viewHome(Request $request, Application $app){
        echo '<!DOCTYPE html>
                <html lang="fr" ng-app="dataWebApp">
                    <head>
                        <meta charset="UTF-8">
                        <title>Données pour le WEB MongoDB</title>
                        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
                        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
                        <link rel="stylesheet" href="webroot/css/style.css">
                    </head>
                    <body>
                        <header>
                            <h1>Gestion de données pour le WEB<small> MongoDB</small></h1>
                        </header>
                        <section ng-view></section>
                        <footer>
                            Licence CISIIE - Oo2015oO - JEROME Cyril
                        </footer>
                    </body>
                    <script src="webroot/lib/angular.js"></script>
                    <script src="https://code.angularjs.org/1.3.15/angular-route.js"></script>
                    <script src="https://code.angularjs.org/1.3.15/angular-resource.js"></script>
                    <script src="webroot/angular/dataWebApp.js"></script>
                    <script src="webroot/angular/controller/indexCtrl.js"></script>
                    <script src="webroot/angular/service/indexService.js"></script>
                </html>';
        return false;
    }

    /**
     * @param $id_family
     * @param $app
     * @return object JSON
     */
    public function getArticlesByFamily($id_family, $app) {
//        récupérer tous les articles en fonction de l'id d'une famille
        $m = new \MongoClient();
        $cursor = $m->catalogue->families->find(array('id' => intval($id_family)), array('articles' => 1));
        $array = iterator_to_array($cursor);
//        mise en forme des données
        foreach ($array as $article){
            foreach ($article['articles'] as $art){
                $reference = $art['reference'];
                $label = $art['label'];
                $pu_ht = $art['pu_ht'];
                $tva = $art['tva'];
                $marque = $art['marque'];
                $tabArt = array('reference' => $reference, 'label' => $label, 'prix' => $pu_ht, 'tva' => $tva, 'marque' => $marque);
                $tabArticles[] = $tabArt;
            }
        }

        return $app->json($tabArticles);

    }

    /**
     * @param $depth
     * @param $app
     * @return Object JSON
     */
    public function FamilyByDepth($depth, $app) {

        $m = new \MongoClient();

        $famDepth = $m->catalogue->families->find(array('depth' => intval($depth)));
        $array = iterator_to_array($famDepth, false);
        foreach ($array as $family){
            $id = intval($family['id']);
            $label = $family['label'];
            $depth = intval($family['depth']);
            $id_parent = array_splice($family['parents'], -1);
            $tabFam = array('id' => $id, 'label' => $label, 'depth' => $depth, 'id_parent' => intval($id_parent['0']['id']));
            $tabFamilies[] = $tabFam;
        }


        return $app->json($tabFamilies);
    }
}